

document.querySelector('#pause')
    .addEventListener('click', function () {
        noLoop();
    });

document.querySelector('#start')
    .addEventListener('click', function () {
        loop()
    });


document.querySelector('#reset-game')
    .addEventListener('click', function () {
        frameCount = 0
        init()
        draw()
    });

document.querySelector('#next')
    .addEventListener('click', function () {
        loop()
        noLoop()
    });

document.querySelector('#random')
    .addEventListener('click', function () {
        randomly()
        loop()
    });


//-------------------Stoke--------------------------
//**************************************************
let stokeTurn = 0
let stoke = document.querySelector("#stroke")
stoke.addEventListener('click', function () {
    if (stokeTurn % 2 == 0) {
        strokeColor = 255
        stoke.classList.replace("btn-primary", "btn-Secondary")
    } else {
        strokeColor = 50
        stoke.classList.replace("btn-Secondary", "btn-primary")
    }
    stokeTurn++
    loop()
})
//-----------------------------------------------

//--------------DeleteCreateCell--------------------
//**************************************************
let turnOfDeleteCreate = 0
let deleteCell = document.querySelector('#deleteCell')

deleteCell.addEventListener('click', function () {
    creatCell.classList.replace("btn-primary", "btn-Secondary")
    deleteCell.classList.replace("btn-Secondary", "btn-primary")
    selectModel = false
    model.classList.replace("btn-primary", "btn-Secondary")
    modelTurn = 0
    if (turnOfDeleteCreate % 2 == 0) {
        turnOfDeleteCreate++
    }
});


let creatCell = document.querySelector('#CreateCell')

creatCell.addEventListener('click', function () {
    deleteCell.classList.replace("btn-primary", "btn-Secondary")
    creatCell.classList.replace("btn-Secondary", "btn-primary")
    if (turnOfDeleteCreate % 2 != 0) {
        turnOfDeleteCreate++
    }
})
//------------------------------------------------







//------------------select model----------------------------
//**************************************************
let modelTurn = 0
let model = document.querySelector('#model')

model.addEventListener("click", function () {
    if (modelTurn % 2 == 0) {
        selectModel = true
        model.classList.replace("btn-Secondary", "btn-primary")
        deleteCell.classList.replace("btn-primary", "btn-Secondary")
        modelTurn++
    } else {
        selectModel = false
        model.classList.replace("btn-primary", "btn-Secondary")
        modelTurn++
    }

})
//------------------------------------------------------------------


//--------click event to control broad size-------------------
//**************************************************
let arrowUp = document.querySelector("#arrowUp")
let arrowDown = document.querySelector("#arrowDown")
let arrowLeft = document.querySelector("#arrowLeft")
let arrowRight = document.querySelector("#arrowRight")



arrowUp.addEventListener("click",function(){
        gameHeight += 0.1
    setupLoopNoLoop()
})

arrowDown.addEventListener("click",function(){
        gameHeight -= 0.1
    setupLoopNoLoop()
})

arrowLeft.addEventListener("click",function(){
        gameWidth += 0.1
    setupLoopNoLoop()
})

arrowRight.addEventListener("click",function(){
        gameWidth -= 0.1
    setupLoopNoLoop()
})