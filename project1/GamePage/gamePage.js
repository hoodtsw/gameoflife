let fr = 5
let frameCount = 0
const unitLength = 20;
const boxColor = 150;
let strokeColor = 50;
let showNeighbors = false
let boxColorFp
let dieOfLoneliness = 2
let dieOfOverpopulation = 3
let newLifeReproduction = 3

let selectModel = false

let darkBoxColor
let darkRgb1 = 255
let darkRgb2 = 128
let darkRgb3 = 0

let rgb1 = 255
let rgb2 = 255
let rgb3 = 102
let columns; /* To be determined by window width */
let rows;    /* To be determined by window height */
let currentBoard;
let nextBoard;

let gameWidth = 1.2
let gameHeight = 1.5



function windowResized() {
    resizeCanvas(windowWidth / gameWidth, windowHeight / gameHeight);
    frameCount = 0
  }



function setup() {
    frameRate(fr)

    const canvas = createCanvas(windowWidth / gameWidth, windowHeight / gameHeight);
    canvas.parent(document.querySelector('#canvas'));

    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    init();
}


function draw() {
    cursor(CROSS)
    background(255);
    FrameCount.innerHTML = frameCount
    boxColorFp = color(rgb1, rgb2, rgb3)
    darkBoxColor = color(darkRgb1, darkRgb2, darkRgb3)
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {

            if (currentBoard[i][j] > 3) {
                if (colorList.value == 0) {
                    stabLifeColor("yellow")
                    fill(darkBoxColor)
                }
                if (colorList.value == 1) {
                    stabLifeColor("blue")
                    fill(darkBoxColor)
                }
                if (colorList.value == 2) {
                    stabLifeColor("red")
                    fill(darkBoxColor)
                }
                if (colorList.value == 3) {
                    stabLifeColor("green")
                    fill(darkBoxColor)
                }

                // fill(color(255, 0, 255));


            } else if (currentBoard[i][j] <= 3 && currentBoard[i][j] > 0) {
                fill(boxColorFp)

            } else {
                fill(255);
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
    frameCount++

}

function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i == 0 && j == 0) {
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    let nx = (x + i + columns) % columns
                    let ny = (y + j + rows) % rows
                    if (currentBoard[nx][ny] > 0) {
                        neighbors += 1
                    }
                    // neighbors = neighbors + currentBoard[nx][ny];
                    // console.log("i:" ,i)
                    // console.log(`x+i:` , x+i)
                    // console.log("x:", x)
                }
            }

            // Rules of Life
            if (currentBoard[x][y] > 0 &&
                neighbors < dieOfLoneliness) {
                // Die of Loneliness
                nextBoard[x][y] = 0;

            } else if (currentBoard[x][y] > 0 &&
                neighbors > dieOfOverpopulation) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;

            } else if (currentBoard[x][y] == 0 &&
                neighbors == newLifeReproduction) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else if (currentBoard[x][y] == 0) {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];

            } else {
                nextBoard[x][y] = currentBoard[x][y] + 1;
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}



// draw a patten in  the canvas
// just follow the patten to create life
function keyPressed() {
    noLoop()
    if (keyCode === 17) {
        for (let i = 0; i < columns; i++) {
            for (let j = 0; j < rows; j++) {
                fill(255)
                rect(i * unitLength, j * unitLength, unitLength, unitLength);
                if ((j % 5 == 1 || i % 5 == 1) &&
                    i + j > 26 && i - j < 21 && j - i < 20 &&
                    i + j < 60 && j < 22 && i < 27 && i > 10) {
                    fill("yellow")
                    rect(i * unitLength, j * unitLength, unitLength, unitLength);
                }

            }
        }
    }
    if (keyCode === 38) {
        gameHeight += 0.1
        setupLoopNoLoop()
    }
    if (keyCode === 40) {
        gameHeight -= 0.1
        setupLoopNoLoop()
    }
    if (keyCode === 37) {
        gameWidth += 0.1
        setupLoopNoLoop()
    }
    if (keyCode === 39) {
        gameWidth -= 0.1
        setupLoopNoLoop()
    }


}

function setupLoopNoLoop() {
    setup()
    loop()
    noLoop()
}




function mouseDragged() {

    // if delete button open , don't draw anything
    // instead, the lifeCell will be clear when it was dragged 
    if (turnOfDeleteCreate % 2 == 1) {
        if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
            return;
        }
        const x = Math.floor(mouseX / unitLength);
        const y = Math.floor(mouseY / unitLength);
        currentBoard[x][y] = 0;
        fill(255);
        stroke(strokeColor);
        rect(x * unitLength, y * unitLength, unitLength, unitLength);
        return
    }

    // if delete button was closed
    // the boxCell was created by dragging
    /**
    * If the mouse coordinate is outside the board
    */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}


function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
    dieOfLoneliness = 2
    dieOfOverpopulation = 3
    newLifeReproduction = 3
    input1.value = 2
    input2.value = 3
    input3.value = 3
    selectModel = false
    model.classList.replace("btn-primary", "btn-Secondary")
    modelTurn = 0
    noLoop()
}

function randomly() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = random() > 0.8 ? 1 : 0;
            nextBoard[i][j] = 0;
        }
    }
}

//------fPS------------
const FrameCount = document.querySelector("#FrameCount")
let FpsSlider = document.querySelector("#myRange")
let FPS = document.querySelector("#FPS")
FPS.innerHTML = FpsSlider.value

//the interaction of fpsCountNumber ,fPS, FpsSlider's value
//FpsSlider's value changed when it was dragged
//FpsSlider's value change fr and fpsCountNumber
FpsSlider.oninput = function () {
    FPS.innerHTML = this.value;
    fr = FpsSlider.value
    frameRate(parseInt(fr))
}
// -----------------------------------------

function mouseClicked() {
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    if (turnOfDeleteCreate % 2 != 0) {
        currentBoard[x][y] = 0;
        fill(255);
    } else {
        currentBoard[x][y] = 1
        fill(boxColor)
    }
    if (selectModel === true) {
        currentBoard[x][y] = 1
        currentBoard[x][y + 1] = 1
        currentBoard[x][y + 2] = 1
        currentBoard[x - 1][y + 2] = 1
        currentBoard[x - 2][y] = 1
        fill(color(192, 96, 233))

        rect(x * unitLength, (y + 1) * unitLength, unitLength, unitLength);
        rect(x * unitLength, (y + 2) * unitLength, unitLength, unitLength);
        rect((x - 1) * unitLength, (y + 2) * unitLength, unitLength, unitLength);
        rect((x - 2) * unitLength, y * unitLength, unitLength, unitLength);
    }

    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);

}













