let colorList = document.querySelector("#colorList")
// let selectedColor = colorList.options[colorList.selectedIndex].text
// let option = document.querySelector("option")


//selectBox's value change the cell color in the canvas
colorList.addEventListener("input", function () {
    if (colorList.value == 0) {
        rgb1 = 255, rgb2 = 255, rgb3 = 102
    } else if (colorList.value == 1) {
        rgb1 = 153, rgb2 = 204, rgb3 = 255
    } else if (colorList.value == 2) {
        rgb1 = 255, rgb2 = 102, rgb3 = 102
    } else {
        rgb1 = 153, rgb2 = 255, rgb3 = 153
    }
})


// while click optionBox, select value changed
// selectBox change color by class according the optionBox's value
colorList.addEventListener('click', function () {

    if (colorList.value == 0) {
        colorList.className = "yellowBoxColor"

    } else if (colorList.value == 1) {
        colorList.className = "blueBoxColor"

    } else if (colorList.value == 2) {
        colorList.className = "redBoxColor"

    } else {
        colorList.className = "greenBoxColor"

    }
})


// each draw() will change the color of stable life
function stabLifeColor(color){
    if (color == 'yellow'){
        darkRgb1 = 255
        darkRgb2 = 128
        darkRgb3 = 0
        return
    }
    if (color == 'blue'){
        darkRgb1 = 0
        darkRgb2 = 128
        darkRgb3 = 255
        return
    }
    if (color == 'red'){
        darkRgb1 = 153
        darkRgb2 = 0
        darkRgb3 = 0
        return
    }
    if (color == 'green'){
        darkRgb1 = 0
        darkRgb2 = 153
        darkRgb3 = 0
        return
    }
    
}


