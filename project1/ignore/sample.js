let fr = 5
let frameCount = 0
const unitLength = 20;
const boxColor = 150;
let strokeColor = 50;
let showNeighbors = false
let boxColorFp
let dieOfLoneliness = 2
let dieOfOverpopulation = 3
let newLifeReproduction = 3

let selectModel = false

let darkBoxColor
let darkRgb1 = 255
let darkRgb2 = 128
let darkRgb3 = 0

let rgb1 = 255
let rgb2 = 255
let rgb3 = 102
let columns; /* To be determined by window width */
let rows;    /* To be determined by window height */
let currentBoard;
let nextBoard;

function setup() {
    frameRate(fr)

    const canvas = createCanvas(windowWidth / 2, windowHeight / 1.5);
    canvas.parent(document.querySelector('#canvas'));

    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    init();
}


function draw() {
    cursor(CROSS)
    background(255);
    FrameCount.innerHTML = frameCount
    boxColorFp = color(rgb1, rgb2, rgb3)
    darkBoxColor = color(darkRgb1, darkRgb2, darkRgb3)
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {

            if (currentBoard[i][j] > 3) {
                if (colorList.value == 0) {
                    stablelifeColor("yellow")
                    fill(darkBoxColor)
                }
                if (colorList.value == 1) {
                    stablelifeColor("blue")
                    fill(darkBoxColor)
                }
                if (colorList.value == 2) {
                    stablelifeColor("red")
                    fill(darkBoxColor)
                }
                if (colorList.value == 3) {
                    stablelifeColor("green")
                    fill(darkBoxColor)
                }

                // fill(color(255, 0, 255));


            } else if (currentBoard[i][j] <= 3 && currentBoard[i][j] > 0) {
                fill(boxColorFp)

            } else {
                fill(255);
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
    frameCount++

}

function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i == 0 && j == 0) {
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    let nx = (x + i + columns) % columns
                    let ny = (y + j + rows) % rows
                    if (currentBoard[nx][ny] > 0) {
                        neighbors += 1
                    }
                    // neighbors = neighbors + currentBoard[nx][ny];
                    // console.log("i:" ,i)
                    // console.log(`x+i:` , x+i)
                    // console.log("x:", x)
                }
            }

            // Rules of Life
            if (currentBoard[x][y] > 0 &&
                neighbors < dieOfLoneliness) {
                // Die of Loneliness
                nextBoard[x][y] = 0;

            } else if (currentBoard[x][y] > 0 &&
                neighbors > dieOfOverpopulation) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;

            } else if (currentBoard[x][y] == 0 &&
                neighbors == newLifeReproduction) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else if (currentBoard[x][y] == 0) {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];

            } else {
                nextBoard[x][y] = currentBoard[x][y] + 1;
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

function keyPressed() {
    if (keyCode === UP_ARROW) {
        background('yellow');
    }
}

function mouseDragged() {

    // if delete button open , don't draw anything
    if (turnOfDeleteCreate % 2 == 1) {
        return
    }


    /**
    * If the mouse coordinate is outside the board
    */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}


function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
    dieOfLoneliness = 2
    dieOfOverpopulation = 3
    newLifeReproduction = 3
    input1.value = 2
    input2.value = 3
    input3.value = 3
    selectModel = false
    model.classList.replace("btn-primary", "btn-Secondary")
    modelTurn = 0
    noLoop()
}

function randomly() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = random() > 0.8 ? 1 : 0;
            nextBoard[i][j] = 0;
        }
    }
}

//-------------------fPS------------
const FrameCount = document.querySelector("#FrameCount")
let FpsSlider = document.querySelector("#myRange")
let FPS = document.querySelector("#FPS")
FPS.innerHTML = FpsSlider.value

//the relation of counter ,fPS, FpsSlider's value
FpsSlider.oninput = function () {
    FPS.innerHTML = this.value;
    fr = FpsSlider.value
    frameRate(parseInt(fr))
}
// -----------------------------------------

function mouseClicked() {
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    if (turnOfDeleteCreate % 2 != 0) {
        currentBoard[x][y] = 0;
        fill(255);
    } else {
        currentBoard[x][y] = 1
        fill(boxColor)
    }
    if (selectModel === true) {
        currentBoard[x][y] = 1
        currentBoard[x][y + 1] = 1
        currentBoard[x][y + -1] = 1
        
            fill(color(200, 200, 200))
            rect(x * unitLength, y * unitLength, unitLength, unitLength)
        
            
            // fill(color(255, 204, 255))
            // rect(x * unitLength, (y - 1) * unitLength, unitLength, unitLength)
            // rect(x * unitLength, (y + 1) * unitLength, unitLength, unitLength)
        
        fill(color(10, 10, 10))
        rect(x * unitLength, (y - 1) * unitLength, unitLength, unitLength)
        rect(x * unitLength, (y + 1) * unitLength, unitLength, unitLength)
        
        // for (let i = 0; i < columns; i++) {
        //     for (let j = 0; j < columns; j++) {

        //     }
        // }
        // rect(x * unitLength, (y - 1) * unitLength, unitLength, unitLength);
        // rect(x * unitLength, (y + 1) * unitLength, unitLength, unitLength);
    }

    stroke(strokeColor);
    fill(color(200, 200, 200))
    rect(x * unitLength, y * unitLength, unitLength, unitLength);

}













